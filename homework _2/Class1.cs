﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework__2
{
    class Cdf
    {

        public static double Phi(double x)//this is a method to calculate cdf for standerd normal distribution
        {
            // constants
            double a1 = 0.254829592;
            double a2 = -0.284496736;
            double a3 = 1.421413741;
            double a4 = -1.453152027;
            double a5 = 1.061405429;
            double p = 0.3275911;

            // Save the sign of x
            int sign = 1;
            if (x < 0)
                sign = -1;
            x = Math.Abs(x) / Math.Sqrt(2.0);

            // A&S formula 7.1.26
            double t = 1.0 / (1.0 + p * x);
            double y = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Math.Exp(-x * x);

            return 0.5 * (1.0 + sign * y);
        }

        static void TestPhi()
        {
            // Select a few input values
            double[] x =
            {
        -3,
        -1,
        0.0,
        0.5,
        2.1
    };

            // Output computed by Mathematica
            // y = Phi[x]
            double[] y =
            {
        0.00134989803163,
        0.158655253931,
        0.5,
        0.691462461274,
        0.982135579437
    };

            double maxError = 0.0;
            for (int i = 0; i < x.Length; ++i)
            {
                double error = Math.Abs(y[i] - Phi(x[i]));
                if (error > maxError)
                    maxError = error;
            }

            Console.WriteLine("Maximum error: {0}", maxError);
        }
    }
}
