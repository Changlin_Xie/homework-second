﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace homework__2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        //use to decide which method to compute
        bool Deltac = false;
        bool Isanti = false;
        private void IsAnti_Checked(object sender, RoutedEventArgs e)
        {
            Isanti = true;
            Deltac = false;
        }
        private void IsAnti_Copy_Checked(object sender, RoutedEventArgs e)
        {
            Isanti = false;
            Deltac = true;
        }
       
        private void Delta_Checked(object sender, RoutedEventArgs e)
        {
            Deltac = true;
            Isanti = true;
        }

        private void NoDelta_Checked(object sender, RoutedEventArgs e)
        {
            Deltac = false;
            Isanti = false;
        }
        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //use to time count
            watch.Reset();
            timecount.Content = "00:00:00:00";
            status.Text = "";
           
            watch.Start();
            timecount.Content = "It is counting now......";

            if (step.Text == "" || Times.Text == "" || Stockprice.Text == "" || Strikprice.Text == "" || rate.Text == "" || Tenor.Text == "" || vol.Text=="")
            {
                status.Text = "the input is not enough!!!";
            }
            else if (status.Text == "input error!")
            {
                status.Text = "need reinput the parameters";
            }
           //THESE ARE AIMED FOR USER TO NOT PUTINTO WRONG INPUTS
            else
            {


                int t = Convert.ToInt32(step.Text);
                int n = Convert.ToInt32(Times.Text);
                double S = Convert.ToDouble(Stockprice.Text);
                double K = Convert.ToDouble(Strikprice.Text);
                double r = Convert.ToDouble(rate.Text);
                double T = Convert.ToDouble(Tenor.Text);
                double sigma = Convert.ToDouble(vol.Text);

                double[,] ran = RandomProduce.GetrandP(n, t);
                //accept the input
                EuOption option1 = new EuOption();
                double[] price = option1.GetPrice(t, n, S, K, r, T, sigma, ran, Isanti,Deltac);
                double[] greek = option1.GetGreek(t, n, S, K, r, T, sigma, ran, Isanti,Deltac);
                //calculate the result
                Callprice.Text = Convert.ToString(price[0]);
                Calldelta.Text = Convert.ToString(greek[0]);
                Callgamma.Text = Convert.ToString(greek[1]);
                Calltheta.Text = Convert.ToString(greek[2]);
                Callrho.Text = Convert.ToString(greek[3]);
                Callvega.Text = Convert.ToString(greek[4]);
                Putprice.Text = Convert.ToString(price[2]);
                Putdelta.Text = Convert.ToString(greek[5]);
                Putgamma.Text = Convert.ToString(greek[6]);
                Puttheta.Text = Convert.ToString(greek[7]);
                Putrho.Text = Convert.ToString(greek[8]);
                Putvega.Text = Convert.ToString(greek[9]);
                CallSE.Text = Convert.ToString(price[1]);
                PutSE.Text = Convert.ToString(price[3]);
                //out put the result
                status.Text = "get the result";
            }
        }

      

        private void Stockprice_TextChanged(object sender, TextChangedEventArgs e)
        {
           // THESE ARE AIMED FOR USER TO NOT PUTINTO WRONG INPUTS

            double num;
            if (!double.TryParse(Stockprice.Text, out num))
            {
                status.Text = "please enter a positive int number";
            }
            else if (Stockprice.Text.Contains("-"))
            {

                status.Text = "input error!";
            }
            else
            {

                status.Text = "";
            }
        }

        private void Strikprice_TextChanged(object sender, TextChangedEventArgs e)
        {// THESE ARE AIMED FOR USER TO NOT PUTINTO WRONG INPUTS
            double num;
            if (!double.TryParse(Strikprice.Text, out num))
            {
                status.Text = "please enter a positive int number";
            }
            else if (Strikprice.Text.Contains("-"))
            {

                status.Text = "input error!";
            }
            else
            {

                status.Text = "";
            }
        }

        private void Tenor_TextChanged(object sender, TextChangedEventArgs e)
        {// THESE ARE AIMED FOR USER TO NOT PUTINTO WRONG INPUTS
            double num;
            if (!double.TryParse(Tenor.Text, out num))
            {
                status.Text = "please enter a positive int number";
            }
            else if (Tenor.Text.Contains("-"))
            {

                status.Text = "input error!";
            }
            else
            {

                status.Text = "";
            }
        }

        private void rate_TextChanged(object sender, TextChangedEventArgs e)
        {// THESE ARE AIMED FOR USER TO NOT PUTINTO WRONG INPUTS
            double num;
            if (!double.TryParse(rate.Text, out num))
            {
                status.Text = "please enter a positive int number";
            }
            else if (rate.Text.Contains("-"))
            {

                status.Text = "input error!";
            }
            else
            {

                status.Text = "";
            }
        }
        private void vol_TextChanged(object sender, TextChangedEventArgs e)
        {
            // THESE ARE AIMED FOR USER TO NOT PUTINTO WRONG INPUTS
            double num;
            if (!double.TryParse(vol.Text, out num))
            {
                status.Text = "please enter a positive int number";
            }
            else if (vol.Text.Contains("-"))
            {

                status.Text = "input error!";
            }
            else
            {

                status.Text = "";
            }
        }

        private void step_TextChanged(object sender, TextChangedEventArgs e)
        {
            // THESE ARE AIMED FOR USER TO NOT PUTINTO WRONG INPUTS
            double num;
            if (!double.TryParse(step.Text, out num))
            {
                status.Text = "please enter a positive int number";
            }
            else if (step.Text.Contains("-"))
            {

                status.Text = "input error!";
            }
            else
            {

                status.Text = "";
            }
        }

        private void Times_TextChanged(object sender, TextChangedEventArgs e)
        {// THESE ARE AIMED FOR USER TO NOT PUTINTO WRONG INPUTS
            double num;
            if (!double.TryParse(Times.Text, out num))
            {
                status.Text = "please enter a positive int number";
            }
            else if (Times.Text.Contains("-"))
            {

                status.Text = "input error!";
            }
            else
            {

                status.Text = "";
            }
        }
        System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
        private void timestart_Click(object sender, RoutedEventArgs e)
        {
            watch.Start();
            timecount.Content = "It is counting now......";
        }

        private void timestop_Click(object sender, RoutedEventArgs e)
        {
            watch.Stop();
            timecount.Content = watch.Elapsed.Hours.ToString()
                + ":"
                + watch.Elapsed.Minutes.ToString()
                + ":"
                + watch.Elapsed.Seconds.ToString()
                + ":"
                + watch.Elapsed.Milliseconds.ToString();
            watch.Reset();
            timecount.Content = "00:00:00:00";
        }

        private void timereset_Click(object sender, RoutedEventArgs e)
        {
            watch.Reset();
            timecount.Content = "00:00:00:00";
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            watch.Start();
            timecount.Content = "It is counting now......";
        }

        private void status_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (status.Text == "get the result")
            {
                watch.Stop();
                timecount.Content = watch.Elapsed.Hours.ToString()
                    + ":"
                    + watch.Elapsed.Minutes.ToString()
                    + ":"
                    + watch.Elapsed.Seconds.ToString()
                    + ":"
                    + watch.Elapsed.Milliseconds.ToString();
            }
        }
    }
}
